from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('', views.home,name='home'),
    #save api
    path('save',views.save,name='home'),
    #rating the song
    path('song_rating',views.rates,name='rate'),
    #playlist creation
    path('play',views.play,name='playlist'),
    #redirecting site to playlist
    path('redirect',views.direct,name='redirect'),
    #display of users, playlists, songs
    path('display',views.display,name='display')
]