from django.shortcuts import render
import csv
from django.db.models import Avg
from .models import music_meta
from .models import rating
import boto3 as boto3

# Dynamo DB table connection
dynamodb = boto3.resource(service_name='dynamodb', region_name='us-east-1', aws_access_key_id='AKIA6EFS5VIHM2K6QJ7V',
                          aws_secret_access_key='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')
table = dynamodb.Table('music-application')
def simple():
    pass
#default message
# Default home page
def home(request):
    return render(request, 'home.html')

def temp():
    pass
# API for saving csv data into postgres and backing it up to s3
def save(request):
    s3 = boto3.resource(
        service_name='s3',
        region_name='us-east-1',
        aws_access_key_id='AKIA6EFS5VIHM2K6QJ7V',
        aws_secret_access_key='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'
    )
    # df = pd.read_csv('/home/nandha/PycharmProjects/music_app/music1.csv', encoding='latin-1')
    file = open('/home/nandha/PycharmProjects/music_app/music1.csv', encoding='latin-1')
    s3.Bucket('msd-dev-team').upload_file(Filename='/home/nandha/PycharmProjects/music_app/music1.csv',
                                          Key='Nandha/m1.csv')
    csvreader = csv.reader(file)
    rows = []
    flag = 0
    print(csvreader)
    for row in csvreader:
        rows.append(row)
    print(rows)
    l = len(rows)
    print(l)
    for id, tit, name in rows:
        song = music_meta(title=tit, artist=name)
        song.save()
        flag += 1
    if flag == l:
        return render(request, 'result.html', {'result': 'Data inserted successfully!!'})
    else:
        return render(request, 'result.html', {'result': 'No data inserted'})


# API for rating the song
def rates(request):
    data = rating.objects
    # data.values_list('song')
    user = str(request.GET["user"])
    song = str(request.GET["song"])
    rate = request.GET["rating"]
    Rate = rating(song=song, rate=rate, user=user)
    Rate.save()
    if data.filter(song=song).count() != 0:
        avg = data.all().aggregate(Avg('rate'))
        data.all().update(rate=int(avg['rate__avg']))
    return render(request, 'result.html', {'result': 'Data inserted successfully!!'})


# redirecting site to playlist creation API
def direct(request):
    data = music_meta.objects
    return render(request, 'playlist.html', {'data': data})


# Playlist creation API
def play(request):
    user = str(request.GET["user"])
    playlist = str(request.GET["playlist"])
    songs = request.GET["songs"]
    table.put_item(
        Item={
            'music': playlist,
            'songs': songs,
            'user': user,
        }
    )
    return render(request, 'result.html', {'result': 'Data inserted successfully!!'})


# API to display user,songs,playlist available on database
def display(request):
    data = music_meta.objects
    response1 = table.scan(AttributesToGet=['user'])
    response2 = table.scan(AttributesToGet=['music'])
    user = response1['Items']
    playlist = response2['Items']
    user_list = []
    playlist_list = []
    for username in user:
        for value in username:
            user_list.append(username[value])
    for playlistname in playlist:
        for value in playlistname:
            playlist_list.append(playlistname[value])
    return render(request, 'display.html', {'songs': data, 'user': user_list, 'playlist': playlist_list})

# bit-bucket:UGwux7Rf3TxSh7Mx7M8R
