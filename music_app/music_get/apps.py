from django.apps import AppConfig


class MusicGetConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'music_get'
